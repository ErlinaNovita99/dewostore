<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function list()
    {
        return inertia('Product/List', [
            'products' => Product::get()
        ]);
    }

    public function show(Product $product)
    {
        return inertia('Product/Show', [
            'product'   => $product
        ]);
    }
}
