<?php

namespace App\Http\Controllers;

use App\Http\Resources\CartListResources;
use App\Models\Cart;
use App\Models\Paymentmethod;
use App\Models\Product;
use App\Models\Shippingaddress;
use App\Models\Shippingmethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;

class CartController extends Controller
{
    public function store(Request $request)
    {
        try {
            $product    = Product::whereSlug($request->product_slug)->firstOrFail();
            $data       = [
                'user_id'           => Auth::user()->id,
                'product_id'        => $product->id,
                'quantity'          => $request->quantity,
                'amount'            => $product->amount,
                'subtotal'          => $request->quantity * $product->amount
            ];
            Cart::create($data);

            return Redirect::back()->with('success', 'Data sudah dimasukkan kedalam keranjang');
        } catch (\Throwable $th) {
            return Redirect::route('my.carts')->with('success', 'Produk sudah berada dalam keranjang');
        }
    }

    public function my()
    {
        return Inertia::render('Carts/My', [
            'primary'           => Shippingaddress::where(['user_id' => Auth::user()->id, 'primary' => 'yes'])->first(),
            'shipping_address'  => Shippingaddress::whereUser_id(Auth::user()->id)->get(),
            'data'              => CartListResources::collection(Auth::user()->carts),
            'payment_method'    => Paymentmethod::whereUser_id(Auth::user()->id)->first() ?? null,
            'shipping_method'   => Shippingmethod::whereUser_id(Auth::user()->id)->first(),
        ]);
    }

    public function destroy(Cart $cart)
    {
        $cart->delete();
        return Redirect::back()->with('success', 'Data berhasil dihapus dari keranjang');
    }
}
