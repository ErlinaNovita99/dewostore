<?php

namespace App\Http\Controllers;


use App\Exports\TransactionExport;
use Maatwebsite\Excel\Facades\Excel;


class ReportController extends Controller
{
    public function export($month)
    {
        return (new TransactionExport)->forMonth($month)->download('invoices.xlsx');
    }
}
