<?php

namespace App\Http\Controllers;

use App\Models\Paymentmethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class PaymentmethodController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $p = Paymentmethod::whereUser_id(Auth::user()->id)->count();
        if ($p > 0) {
            Paymentmethod::whereUser_id(Auth::user()->id)->update([
                'name'          => $request->name,
                'code'          => $request->code,
                'logo'          => $request->logo
            ]);
        } else {
            Paymentmethod::create([
                'user_id'       => Auth::user()->id,
                'name'          => $request->name,
                'code'          => $request->code,
                'logo'          => $request->logo
            ]);
        }

        return Redirect::back()->with('Metode pembayaran berhasil di pilih');
    }
}
