<?php

namespace App\Exports;

use App\Http\Resources\TransactionListResources;
use App\Models\Transaction;
use Illuminate\Support\Facades\Date;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TransactionExport implements FromQuery, WithHeadings, WithMapping
{
    use Exportable;
    protected $month;
    public function forMonth(string $month)
    {
        $this->month = $month;

        return $this;
    }

    public function query()
    {
        return Transaction::query()->whereMonth('created_at', $this->month);
    }

    public function headings(): array
    {
        return [
            '#',
            'Nama',
            'Produk',
            'Resi',
            'Jumlah',
            'Total Harga',
            'Metode Pembayaran',
            'Status Pembayaran',
            'Dibuat Pada',
        ];
    }

    public function map($data): array
    {
        return [
            $data->id,
            $data->user->name,
            $data->product->title,
            $data->resi,
            $data->qty,
            $data->total,
            $data->paymentmethod_name,
            $data->status === '1' ? 'Belum Dibayar' : ( $data->status === '2' ? 'Transaksi Aktif' : ($data->status === '3' ? 'Transaksi Selesai' : 'Transaksi Dibatalkan')),
            $data->created_at,
        ];
    }
}
