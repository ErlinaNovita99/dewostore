<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shippingaddresses', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(User::class);
            $table->string('recipientname');
            $table->string('phone');
            $table->string('province_code', 11)->nullable();
            $table->string('province_desc')->nullable();
            $table->string('city_code', 11)->nullable();
            $table->string('city_desc')->nullable();
            $table->string('district_code', 11)->nullable();
            $table->string('district_desc')->nullable();
            $table->string('postalcode')->nullable();
            $table->string('address')->nullable();
            $table->enum('primary', ["yes", "no"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shippingaddresses');
    }
};
