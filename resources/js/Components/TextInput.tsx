import classNames from 'classnames';
import React, { forwardRef } from 'react';

const TextInput = forwardRef<
    HTMLInputElement,
    React.DetailedHTMLProps<
        React.InputHTMLAttributes<HTMLInputElement>,
        HTMLInputElement
    >
>((props, ref) => (
    <input
        {...props}
        ref={ref}
        className={classNames(
            `hover:border-sky-500 ${props.disabled ? "bg-slate-100" : ""} border-gray-300 focus:border-blue-300 focus:ring-1/2 focus:ring-blue-200 focus:ring-opacity-50 rounded-lg shadow-sm text-sm py-2.5 lg:py-1.5 lg:text-lg placeholder:text-[11.5pt] placeholder:text-stone-400`,
            props.className,
        )}
    />
));

export default TextInput;
