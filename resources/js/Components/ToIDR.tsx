export function tsxToIDR(data: number) {
    const rp = data?.toString()
    const number = Number(rp.replace(/\D/g, '')); // remove all non-digit characters
    return number.toLocaleString('id-ID'); // format the number using Indonesian locale
}
