import { Select } from 'antd'
import React, { FunctionComponent } from 'react'
import InputError from './InputError'
import InputLabel from './InputLabel'

interface FormSelectProps {
    form: any
    label: string
    name: string
    data: any[]
    value: any
    className?: any
    disabled?: boolean
    showSearch?: boolean
    defaultValue?: any
    placeholder?: string
}

const FormSelect: FunctionComponent<FormSelectProps> = (props) => {

    const handleChange = (value: string) => {
        props.form.setData(props.name, value)
    };
    return (
        <div>
            <InputLabel htmlFor={props.name}>{props.label}</InputLabel>
            <div className='my-1.5'>
                <Select
                    defaultValue={props?.defaultValue}
                    optionFilterProp="children"
                    showSearch={props?.showSearch}
                    filterOption={(input: any, option: any) =>
                        (option?.label ?? '').toLowerCase().includes(input.toLowerCase())
                    }
                    disabled={props?.disabled}
                    className={`${props.className} py-0`}
                    value={props.value || undefined}
                    id={props.name}
                    size='large'
                    style={{ width: "100%" }}
                    onChange={handleChange}
                    options={props.data}
                    placeholder={props?.placeholder}
                />
            </div>
            <InputError className="mt-2" message={props.form.errors[props.name]} />
        </div>
    )
}

export default FormSelect;
