import React, { FunctionComponent } from 'react'
import InputError from './InputError'
import InputLabel from './InputLabel'
import TextInput from './TextInput'

interface FormInputProps {
    form: any
    label: string
    name: string
    type?: string
    autoFocus?: boolean
    required?: boolean
    max?: number
    min?: number
    disabled?: boolean
    placeholder?: string
}

const FormInput: FunctionComponent<FormInputProps> = (props) => {
    return (
        <div>
            <InputLabel htmlFor={props.name}>{props.label}</InputLabel>
            <TextInput
                placeholder={props?.placeholder}
                disabled={props.disabled}
                id={props.name}
                type={props.type ?? "text"}
                className="mt-1 block w-full"
                value={props.form.data[props.name]}
                onChange={e => props.form.setData(props.name, e.currentTarget.value)}
                autoFocus={props.autoFocus}
                required={props.required}
                autoComplete={props.name}
                maxLength={props.max}
                minLength={props.min}
            />
            <InputError className="mt-2" message={props.form.errors[props.name]} />
        </div>
    )
}

export default FormInput;
