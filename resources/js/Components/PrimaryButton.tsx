import classNames from 'classnames';
import React, { PropsWithChildren } from 'react';

type Props = React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
>;

export default function PrimaryButton({
    children,
    ...props
}: PropsWithChildren<Props>) {
    return (
        <button
            {...props}
            className={classNames(
                props?.disabled ? "bg-slate-600 hover:bg-slate-700 text-sm border py-1.5 px-3 rounded-full text-white border-slate-600" :
                    'bg-sky-600 hover:bg-sky-700 text-sm border py-1.5 px-3 rounded-full text-white border-sky-600',
                props.className,
            )}
        >
            {children}
        </button>
    );
}
