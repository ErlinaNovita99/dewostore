import Modal from "@/Components/Modal";
import Carts from "@/Components/Product/Cart";
import { tsxToIDR } from "@/Components/ToIDR";
import BaseLayout from "@/Layouts/BaseLayout";
import { Image } from "antd";
import React, { FunctionComponent, useState } from "react";
import { TbShare } from "react-icons/tb";
import { ImFacebook2, ImTwitter } from "react-icons/im";
import { MdOutlineContentCopy } from "react-icons/md";
import { toast } from "react-hot-toast";
import {
    FacebookShareButton,
    TwitterShareButton,
} from "react-share";


interface ShowProps {
    product: any
}

const Show: FunctionComponent<ShowProps> = (props) => {
    const data = props?.product;
    const currentURL = window.location.href;

    const [modalShare, setModalShare] = useState<boolean>(false)
    const handleCopyClick = () => {
        setModalShare(false)
        toast.success("Url berhasil di copy")
        navigator.clipboard.writeText(currentURL);
    };

    return (
        <BaseLayout>
            <Modal isOpen={modalShare} onClose={() => setModalShare(false)}>
                <div className="p-4">
                    <div className="font-bold text-center">Bagikan Produk Ini</div>
                    <div className="grid grid-cols-3 mt-4">
                        <div className="flex justify-center items-center">
                            <button onClick={handleCopyClick}>
                                <MdOutlineContentCopy className="h-8 w-8 text-slate-600 mx-auto" />
                                <div className="text-center mt-2 mb-4">Salin</div>
                            </button>
                        </div>
                        <div className="flex justify-center items-center">

                            <FacebookShareButton
                                url={currentURL}
                                quote={data?.title}
                                className="Demo__some-network__share-button"
                            >
                                <div>
                                    <ImFacebook2 className="h-8 w-8 text-blue-600 mx-auto" />
                                    <div className="text-center mt-2 mb-4">Facebook</div>
                                </div>
                            </FacebookShareButton>

                        </div>
                        <div className="flex justify-center items-center">
                            <TwitterShareButton
                                url={currentURL}
                                title={data?.title}
                                className="Demo__some-network__share-button"
                            >
                                <div>
                                    <ImTwitter className="h-8 w-8 text-sky-600 mx-auto" />
                                    <div className="text-center mt-2 mb-4">Twitter</div>
                                </div>
                            </TwitterShareButton>

                        </div>
                    </div>
                </div>
            </Modal>
            <div className="grid grid-cols-4 gap-8">
                <div>
                    <Image preview src={data?.image} alt="img" style={{width: "250px"}} className="rounded-lg" />
                </div>
                <div className="col-span-2">
                    <div className="flex justify-between items-center">
                        <div>
                            <div className="font-bold text-xl">{data?.title}</div>
                        </div>
                        <button onClick={() => setModalShare(!modalShare)}>
                            <TbShare className="w-5 h-5 text-sky-600" />
                        </button>
                    </div>
                    <div>{data?.subtitle}</div>
                    <div className="mt-2">Harga : Rp. {tsxToIDR(data?.amount)}</div>
                    <div className="mt-4 pt-4 border-t">{data?.description}</div>
                </div>
                <Carts slug={data?.slug} stock={data?.stock} amount={data?.amount} />
            </div>
        </BaseLayout>
    );
}

export default Show;
