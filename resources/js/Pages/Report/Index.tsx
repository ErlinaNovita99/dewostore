import FormSelect from '@/Components/FormSelect'
import PrimaryButton from '@/Components/PrimaryButton'
import useRoute from '@/Hooks/useRoute'
import BaseLayout from '@/Layouts/BaseLayout'
import { InertiaLink, Link, useForm } from '@inertiajs/inertia-react'
import classNames from 'classnames'
import React from 'react'
import { TbLogout } from 'react-icons/tb'

const Index = () => {
    const route = useRoute();
    const form = useForm({
        'month': '01',
    });
    return (
        <BaseLayout>
            <div className="grid grid-cols-3 gap-4">
                <div className="bg-white shadow-lg rounded-lg p-4">
                    <Link href="/transaction/history">
                        <div className="font-bold text-slate-600">Pesanan Saya</div>
                    </Link>
                    <Link href="/user/profile">
                        <div className="font-bold text-slate-600 my-4">Akun Saya</div>
                    </Link>
                    <Link href="/laporan-pesanan">
                        <div className="font-bold text-blue-600 my-4">Laporan Pesanan</div>
                    </Link>
                    <InertiaLink
                        href={route('logout')}
                        method="post"
                        as="button"
                    >
                        <div className="flex items-center gap-2 mt-10">
                            <TbLogout className="w-5 h-5 text-red-600" />
                            <div className="text-red-600">Keluar Akun</div>
                        </div>
                    </InertiaLink>
                </div>
                
                <div className="col-span-2 bg-white shadow-lg rounded-lg p-4">
                    <div className="font-bold capitalize">Laporan Pesanan</div>
                    <div className='my-2 flex justify-start gap-6 items-end mt-10'>
                        <div className='w-72'>
                            <FormSelect
                                form={form}
                                value={form.data.month}
                                placeholder='Pilih bulan transaksi'
                                data={[
                                    {
                                        value: '01',
                                        label: 'Januari'
                                    },
                                    {
                                        value: '02',
                                        label: 'Februari'
                                    },
                                    {
                                        value: '03',
                                        label: 'Maret'
                                    },
                                    {
                                        value: '04',
                                        label: 'April'
                                    },
                                    {
                                        value: '05',
                                        label: 'Mei'
                                    },
                                    {
                                        value: '06',
                                        label: 'Juni'
                                    },
                                    {
                                        value: '07',
                                        label: 'Juli'
                                    },
                                    {
                                        value: '08',
                                        label: 'Agustus'
                                    },
                                    {
                                        value: '09',
                                        label: 'September'
                                    },
                                    {
                                        value: '10',
                                        label: 'Oktober'
                                    },
                                    {
                                        value: '11',
                                        label: 'November'
                                    },
                                    {
                                        value: '12',
                                        label: 'Desember'
                                    },
                                ]}
                                name="month"
                                label="Bulan Transkasi"
                            />
                        </div>
                        <div>
                            <a href={`/users/export/${form?.data?.month}`}>
                                <div className="font-bold py-2 px-4 rounded-full bg-blue-700 text-white mb-2">Download Report</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </BaseLayout >

    )
}

export default Index
