import PrimaryButton from "@/Components/PrimaryButton";
import SecondaryButton from "@/Components/SecondaryButton";
import BaseLayout from "@/Layouts/BaseLayout";
import { Link } from "@inertiajs/inertia-react";
import React, { FunctionComponent } from "react";
import { BsFillPatchCheckFill } from "react-icons/bs";

interface ThanksProps {
    resi: any,
    date: any,
    methode: any,
}

const Thanks: FunctionComponent<ThanksProps> = (props) => {
    return (
        <BaseLayout>
            <div className="max-w-lg mx-auto p-4 bg-white mt-6 rounded-lg shadow-lg">
                <div className="flex justify-center my-6">
                    <BsFillPatchCheckFill className="w-20 h-20 text-blue-600" />
                </div>
                <div className="font-bold text-xl text-center">Terima Kasih Telah Bertransaksi di Dewo Store Kabupaten Jombang</div>
                <div className="text-sm text-center mt-5">kami akan memberitahumu ketika barang sudah mulai diantar ke tempat tujuan dan semoga barang selamat sampai tujuan. </div>
                <div className="mt-10 flex justify-between items-center">
                    <div>Metode Transaksi</div>
                    <div>{props?.methode}</div>
                </div>
                <div className="mt-4 flex justify-between items-center">
                    <div>Tanggal Transaksi</div>
                    <div>{props?.date}</div>
                </div>
                <div className="mt-4 flex justify-between items-center">
                    <div>No. Pemesanan</div>
                    <div>{props?.resi}</div>
                </div>
                <div className="mt-6">
                    <Link href="/transaction/history">
                        <SecondaryButton className="w-full">Lihat Daftar Transaksi</SecondaryButton>
                    </Link>
                </div>
                <div className="mt-2">
                    <Link href="/">
                        <PrimaryButton className="w-full">Belanja Lagi</PrimaryButton>
                    </Link>
                </div>
            </div>
        </BaseLayout>
    );
}

export default Thanks;
