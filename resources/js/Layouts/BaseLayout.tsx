import useRoute from "@/Hooks/useRoute";
import useTypedPage from "@/Hooks/useTypedPage";
import { Head, InertiaLink, Link, usePage } from "@inertiajs/inertia-react";
import React, { FunctionComponent, useEffect } from "react";
import { TbHistory, TbShoppingBag } from "react-icons/tb";
import toast, { Toaster } from 'react-hot-toast';
import Right from "./Right";

interface BaseLayoutProps {
    children: React.ReactNode
}

const BaseLayout: FunctionComponent<BaseLayoutProps> = (props) => {
    const route = useRoute();
    const page = useTypedPage();

    const { flash, carts }: any = usePage().props;
    useEffect(() => {
        if (flash?.success) {
            toast.success(flash?.success);
        }
        if (flash?.error != null) {
            toast.error(flash?.error);
        }
    }, [flash]);

    return (
        <>
            <Head title="Welcome" />
            <Toaster position="top-center" reverseOrder={false} />
            <div className='bg-white sticky top-0 py-3 w-full shadow-md flex items-center'>
                <div className='max-w-7xl mx-auto flex justify-between items-center w-full'>
                    <div>
                        <Link href="/">
                            <div className="flex justify-start items-center gap-4">
                                <img src="/logo-2.png" alt="logo" className="w-9 h-9" />
                                <div className="font-bold capitalize mt-1 text-lg text-sky-600">DEWO STORE</div>
                            </div>
                        </Link>
                    </div>
                    <div>
                        <div className="flex justify-end gap-2 items-center">
                            {page.props.user ? (
                                <>
                                    <Link href={route('transaction.history')}>
                                        <div className="flex items-center">
                                            <TbHistory className="w-5 h-5" />
                                            <sup>
                                                <div className="bg-red-300 w-2 h-2 rounded-full"/>
                                            </sup>
                                        </div>
                                    </Link>
                                    <Link href={route('my.carts')}>
                                        <div className="flex items-center">
                                            <TbShoppingBag className="w-5 h-5" />
                                            <sup>
                                                <div className="bg-red-300 p-2 rounded-full">{carts}</div>
                                            </sup>
                                        </div>
                                    </Link>
                                    <Right />
                                </>
                            ) : (
                                <>
                                    <InertiaLink
                                        href={route('login')}
                                        className="text-sky-700 border border-sky-700 py-1 px-4 rounded-full text-sm"
                                    >
                                        Masuk
                                    </InertiaLink>

                                    <InertiaLink
                                        href={route('register')}
                                        className="bg-sky-700 py-1 px-4 rounded-full border border-sky-700 text-white ml-4 text-sm"
                                    >
                                        Register
                                    </InertiaLink>
                                </>
                            )}
                        </div>
                    </div>
                </div>
            </div>
            <div className="max-w-7xl mx-auto py-10">
                {props?.children}
            </div>
            <div className='bg-sky-200 py-4 p-4 px-10 mt-20'>
                <div className="font-bold text-xl capitalize text-center text-gray-600">Layanan spesial dari kami hanya untukmu.</div>
            </div>
            <div className='bg-slate-50'>
                <div className='max-w-7xl mx-auto grid grid-cols-3 py-10'>
                    <div>
                        <div className='capitalize font-bold text-sky-600'>Tentang Dewo Store</div>
                        <div className='capitalize mt-1'>Tentang Kami</div>
                        <div className='capitalize mt-1'>Toko Kami</div>
                        <div className='capitalize mt-1'>Kerjasama</div>
                    </div>
                    <div>
                        <div className='capitalize font-bold text-sky-600'>Lainnya</div>
                        <div className='capitalize mt-1'>Syarat dan Ketentuan</div>
                        <div className='capitalize mt-1'>Kebijakan dan Privasi</div>
                        <div className='capitalize mt-1'>Bantuan</div>
                        <div className='capitalize mt-1'>Hubungi Kami</div>
                    </div>
                </div>
            </div>
            <div className='bg-sky-200 py-4 flex justify-between items-center p-4 px-10'>
                <div className="flex justify-start items-center gap-4">
                    <img src="/logo-2.png" alt="logo" className="w-9 h-9" />
                    <div className="font-bold capitalize mt-1 text-lg text-sky-600">DEWO STORE</div>
                </div>
                <div className="capitalize text-base text-gray-600">Toko oleh-oleh khas kabupaten jombang</div>
                <div className="flex justify-start items-center gap-4">
                    <img src="/instagram.png" alt="logo" className="w-8 h-8" />
                    <img src="/facebook.png" alt="logo" className="w-8 h-8" />
                    <img src="/twitter.png" alt="logo" className="w-8 h-8" />
                </div>
            </div>
        </>
    );
}

export default BaseLayout;
